<?php
/**
 * @file
 * ethical_taxonomy.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function ethical_taxonomy_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: taxonomy
  $overrides["taxonomy.panopoly_categories.name"] = 'Category';

 return $overrides;
}
